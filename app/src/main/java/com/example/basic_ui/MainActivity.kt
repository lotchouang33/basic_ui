package com.example.basic_ui

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.basic_ui.databinding.ActivityMainBinding
import splitties.activities.start
import splitties.alertdialog.*
import splitties.toast.longToast
import splitties.toast.toast

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
//        setContentView(R.layout.activity_main)
        setSupportActionBar(binding.toolbar)
        //longToast(R.string.msg_launch)
      //  view ->
        //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
          //      .setAction("Action", null).show()
        binding.fab.setOnClickListener {
            showAlertDialog()
        }
        binding.switchSecond.setOnClickListener { changeActivity() }
        binding.shared.setOnClickListener { share(R.string.text_share, R.string.title_share) }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showAlertDialog() {
        alertDialog {
            messageResource = R.string.text_alert
            okButton { sendEmail("lotchouang33@gmail.com", "Hi", "Hello!") }
            cancelButton { Log.i(MainActivity::class.simpleName, "envoi de mail annuler") }
        }.onShow {
            positiveButton.setText(R.string.action_like)
        }.show()
    }

    private fun sendEmail(to: String, subject: String, msg: String) {
        val emailIntent = Intent(Intent.ACTION_SEND)

        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        emailIntent.putExtra(Intent.EXTRA_TEXT, msg)

        try {
            startActivity(Intent.createChooser(emailIntent, getString(R.string.title_send_email)))
        } catch (ex: ActivityNotFoundException) {
            toast(R.string.text_no_email_client)
        }
    }

    fun Context.share(text: Int, subject: Int = R.string.app_name): Boolean {
        try {
            val intent = Intent(android.content.Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(subject))
            intent.putExtra(android.content.Intent.EXTRA_TEXT, getString(text))
            startActivity(Intent.createChooser(intent, null))
            return true
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            return false
        }
    }

    public fun changeActivity()
    {
        start<SecondActivity>()
    }


}