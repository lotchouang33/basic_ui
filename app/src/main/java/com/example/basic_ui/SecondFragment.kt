package com.example.basic_ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.basic_ui.databinding.FragmentSecondBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {
    private lateinit var binding_fragment2 : FragmentSecondBinding
    val url = "chillcoding.com/"
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding_fragment2 = FragmentSecondBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        Toast.makeText(activity,"second fragment", Toast.LENGTH_LONG).show()
        return binding_fragment2.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding_fragment2.buttonSecond.setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }

        binding_fragment2.buttonWeb.setOnClickListener{ browse(url)}
    }
    private fun browse(url: String) {
        var browser = Intent(Intent.ACTION_VIEW, Uri.parse("https://"+url))
        startActivity(browser)
    }
}