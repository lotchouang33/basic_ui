package com.example.basic_ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.basic_ui.databinding.ActivityMainBinding
import com.example.basic_ui.databinding.ActivitySecondBinding
import splitties.activities.start

class SecondActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySecondBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.switchFirst.setOnClickListener { start<MainActivity>() }


    }

}